#ifndef DRAW_H
#define DRAW_H

#include <SDL2/SDL.h>

// Define the size of your tiles in pixels
#define TILE_SIZE 32
#include "data.h"

// Function prototype for drawing game data
void drawGameData(SDL_Renderer *renderer, const GameData *gameData, SDL_Texture *tileTexture, SDL_Texture *unitTexture, SDL_Texture *buildingTexture);

#endif /* DRAW_H */
