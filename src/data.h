#pragma once
#include <stdbool.h>

#define MAX_UNITS 100
#define MAX_BUILDINGS 50
#define MAX_PLAYERS 2
#define MAX_MISSIONS 10
#define MAX_RESOURCES 50
#define MAP_SIZE 100
#define numLEVELS 50

typedef struct {
    int playerID;
    char playerName[50];
    int gold;
    int xp;
} Player;


typedef struct Unit {
    int unitID;
    int playerID;
    int x, y; // Position
    int health;
} Unit;

typedef struct {
    int buildingID;
    int playerID;
    int x, y; // Position
    bool isConstructed;
    int health;
} Building;

typedef struct {
    int mapID;
    char mapName[50];
    int mapWidth;
    int mapHeight;
    uint4_t tileData[MAP_SIZE][MAP_SIZE]; 
    int playerStartingPositions[MAX_PLAYERS][2]; // [x, y] coordinates for each player
    int resourceLocations[MAX_RESOURCES][2]; // [x, y] coordinates for each resource
} Map;

typedef struct {
    int missionID;
    char missionName[50];
    char missionDescription[100];
    bool isCompleted;

    char objectives[5][100]; // Array of objectives with a maximum of 5 objectives

    int experienceReward;
    int goldReward;

    int timeLimit; // Time limit for completing the mission (in seconds)
    bool hasSpecialConditions; // Flag indicating whether the mission has special conditions
} Mission;

typedef struct {
    Player players[MAX_PLAYERS];
    Unit units[MAX_UNITS];
    Building buildings[MAX_BUILDINGS];
    Map currentMap;
    Mission missions[MAX_MISSIONS];
    int mapx, mapy; 
} GameData;

// Structure representing building data
typedef struct {
    int buildingID;
    int maxHealth;
    char name[50];
    int x, y, w, h; // sprite position
    int cost;
} BuildingData;

// Structure representing unit data
typedef struct {
    int unitID;
    int x, y, w, h; // sprite position
    int maxHealth;
    char name[50];        // Name of the unit
    int range; // Type of attack (Melee, Ranged, Magic)
    int damage;           // Damage dealt by the unit
    int lineOfSight;      // Line of sight (LOS) of the unit
    int cost;     // Resource cost to create the unit
} UnitData;

typedef struct {
    GameData data[numLEVELS];
    BuildingData buildings[100];
    UnitData units[100];
} Game;
