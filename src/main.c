#include <SDL2/SDL.h>
#include <stdbool.h>
#include "data.h"
#include "draw.h"
#include "input.h"
#include "ui.h"
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

void updateGameState() {

}

int main(int argc, char* argv[]) {
    SDL_Init(SDL_INIT_VIDEO);
    TTF_Init();
    SDL_Window* window = SDL_CreateWindow("RTS Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                          SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    // Main loop flag
    bool quit = false;
    SDL_Event event;

    // Load textures for tiles, units, and buildings
    SDL_Texture* unitTexture = IMG_LoadTexture(renderer, "path/to/unit_texture.png");
    SDL_Texture* buildingTexture = IMG_LoadTexture(renderer, "path/to/building_texture.png");
    SDL_Texture* tileTexture = IMG_LoadTexture(renderer, "path/to/tile_texture.png");

    // Assuming you have a GameData instance named gameData
    GameData gameData;
    // Initialize gameData here...


    while (!quit) {
        // Event handling
        while (SDL_PollEvent(&event) != 0) {
            if (event.type == SDL_QUIT) {
                quit = true;
            }
            // Handle other events such as key presses, mouse input, etc.
        }

        // Update game state
        updateGameState(); // You need to define this function to update the game state

        // Clear the screen
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        // Render game objects
        drawGameData(renderer, &gameData, tileTexture, unitTexture, buildingTexture);

        // Update the screen
        SDL_RenderPresent(renderer);

        // Cap the frame rate
        SDL_Delay(16); // Cap to approximately 60 frames per second
    }

    // Clean up and exit
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    // Destroy window and renderer
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    // Quit SDL subsystems
    SDL_Quit();

    return 0;
}