#include <SDL2/SDL.h>
#include "draw.h"
#include "data.h"
// Function to draw the game data onto the screen
void drawGameData(SDL_Renderer *renderer, const GameData *gameData, SDL_Texture *tileTexture, SDL_Texture *unitTexture, SDL_Texture *buildingTexture) {
    // Clear the screen
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    // Draw the map tiles
    for (int i = 0; i < gameData->currentMap.mapWidth; i++) {
        for (int j = 0; j < gameData->currentMap.mapHeight; j++) {
            int tileIndex = gameData->currentMap.tileData[i][j];

            // Calculate destination rectangle for the tile
            SDL_Rect destRect = {i * TILE_SIZE, j * TILE_SIZE, TILE_SIZE, TILE_SIZE};

            // Render the tile texture
            SDL_RenderCopy(renderer, tileTexture, NULL, &destRect);
        }
    }

    // Draw units
    for (int i = 0; i < MAX_UNITS; i++) {
        // Check if the unit exists (you may have a different condition)
        if (gameData->units[i].unitID != -1) {
            // Calculate destination rectangle for the unit
            SDL_Rect destRect = {gameData->units[i].x, gameData->units[i].y, TILE_SIZE, TILE_SIZE};

            // Render the unit texture
            SDL_RenderCopy(renderer, unitTexture, NULL, &destRect);
        }
    }

    // Draw buildings
    for (int i = 0; i < MAX_BUILDINGS; i++) {
        // Check if the building exists (you may have a different condition)
        if (gameData->buildings[i].buildingID != -1) {
            // Calculate destination rectangle for the building
            SDL_Rect destRect = {gameData->buildings[i].x, gameData->buildings[i].y, TILE_SIZE, TILE_SIZE};

            // Render the building texture
            SDL_RenderCopy(renderer, buildingTexture, NULL, &destRect);
        }
    }

    // Present the rendered frame
    SDL_RenderPresent(renderer);
}

void drawMinimap(SDL_Renderer *renderer, const Map *map, int minimapX, int minimapY, int minimapSize) {
    // Calculate the size of each minimap tile
    int tileSizeX = minimapSize / map->mapWidth;
    int tileSizeY = minimapSize / map->mapHeight;

    // Iterate through each map tile and draw a simplified representation on the minimap
    for (int i = 0; i < map->mapWidth; i++) {
        for (int j = 0; j < map->mapHeight; j++) {
            SDL_Rect minimapTileRect = {minimapX + i * tileSizeX, minimapY + j * tileSizeY, tileSizeX, tileSizeY};

            // Set color based on the type of tile (for example, green for grass, blue for water, etc.)
            SDL_SetRenderDrawColor(renderer, 0, 255, 0, 255); // Example: Green color for grass tiles

            // Fill the minimap tile
            SDL_RenderFillRect(renderer, &minimapTileRect);
        }
    }

    // Draw borders for the minimap
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_Rect minimapBorderRect = {minimapX, minimapY, minimapSize, minimapSize};
    SDL_RenderDrawRect(renderer, &minimapBorderRect);
}