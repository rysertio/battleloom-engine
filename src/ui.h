#ifndef UI_H
#define UI_H

#include <stdbool.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

// Structure representing a button
typedef struct {
    int x, y, w, h;
    bool clicked;
    void (*onClick)();
    char text[50];
} Button;

// Function prototypes
bool isPointInsideRect(int x, int y, SDL_Rect rect);
void handleButtonClick(SDL_Event *event, Button *button);
void handleButtonEvents(SDL_Event *event, Button buttons[], int numButtons);
void drawButtons(SDL_Renderer *renderer, Button buttons[], int numButtons, TTF_Font *font); 

#endif /* UI_H */
